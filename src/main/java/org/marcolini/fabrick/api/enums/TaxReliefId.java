/*
 * All Rights Reserved License, proprietary and confidential
 *
 * Copyright (c) 2021. Stefano Marcolini - Milano Italy, - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Stefano Marcolini stefano@marcolini.org, 2021.
 *
 * It is not allowed to reuse, modify, or redistribute the Software for
 * commercial use in any way.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS OR APRESS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package org.marcolini.fabrick.api.enums;

import org.marcolini.fabrick.api.exceptions.InvalidParameterException;

import java.util.Arrays;
import java.util.Optional;

import static org.marcolini.fabrick.api.utils.SafeUtil.isNull;

public enum TaxReliefId {

    TR_119R,  //Interventi superbonus.
    TR_L027,  //Bonus facciate.
    TR_DL50,  //Interventi antisismici.
    TR_L090,  //Acquisto mobilio per ristrutturazione.
    TR_L296,  //Risparmio energetico.
    TR_L449;  //Ristrutturazione.

    private static final int THREE = 3;

    public static String validate(String taxReliefId) {
        if (isNull(taxReliefId)) {
            return null;
        }
        Optional<TaxReliefId> optional = Arrays.stream(TaxReliefId.values())
                .filter(x -> taxReliefId.equalsIgnoreCase(x.name().substring(THREE)))
                .findFirst();
        if(optional.isPresent()) {
            return optional.get().name();
        } else {
            throw new InvalidParameterException("Input parameter must be one of TaxReliefId values!");
        }
    }
}
