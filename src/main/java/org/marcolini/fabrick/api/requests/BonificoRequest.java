/*
 * All Rights Reserved License, proprietary and confidential
 *
 * Copyright (c) 2021. Stefano Marcolini - Milano Italy, - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Stefano Marcolini stefano@marcolini.org, 2021.
 *
 * It is not allowed to reuse, modify, or redistribute the Software for
 * commercial use in any way.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS OR APRESS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package org.marcolini.fabrick.api.requests;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.marcolini.fabrick.api.constants.ApiConstants;
import org.marcolini.fabrick.api.enums.FeeType;
import org.marcolini.fabrick.api.models.Creditor;
import org.marcolini.fabrick.api.models.TaxRelief;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.lang.NonNull;

@Getter
@NoArgsConstructor
public final class BonificoRequest {

    private Creditor creditor;      // required
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private String executionDate;   // This field is required unless 'isInstant == true' - format:  2021-08-09
    private String uri;
    private String description;     // required - MAX 140 chars : Payment invoice 75/2017
    private Double amount;          // required : 800,01
    private String currency;        // required : EUR
    private Boolean isUrgent;
    private Boolean isInstant;
    private String feeType;         // one of: SHA, OUR, BEN
    private String feeAccountId;
    private TaxRelief taxRelief;

    public BonificoRequest(@NonNull Creditor creditor, String executionDate, @NonNull String description,
                           @NonNull Double amount, @NonNull String currency) {
        this.creditor = creditor;
        this.executionDate = executionDate;
        this.description = validateDescription(description);
        this.amount = amount;
        this.currency = currency;
    }

    public BonificoRequest(@NonNull Creditor creditor, String executionDate, String uri, @NonNull String description,
                           @NonNull Double amount, @NonNull String currency, Boolean isUrgent,
                           Boolean isInstant, String feeType, String feeAccountId, TaxRelief taxRelief) {
        this.creditor = creditor;
        this.executionDate = executionDate;
        this.uri = uri;
        this.description = validateDescription(description);
        this.amount = amount;
        this.currency = currency;
        this.isUrgent = isUrgent;
        this.isInstant = isInstant;
        this.feeType = FeeType.validate(feeType);
        this.feeAccountId = feeAccountId;
        this.taxRelief = taxRelief;
    }

    private static String validateDescription(String description) {
        return description != null && description.length() > ApiConstants.NUM_140
                ? description.substring(0, ApiConstants.NUM_140)
                : description;
    }
}
