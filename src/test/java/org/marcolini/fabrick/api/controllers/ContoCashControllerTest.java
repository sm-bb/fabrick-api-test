package org.marcolini.fabrick.api.controllers;

import org.junit.jupiter.api.Test;
import org.marcolini.fabrick.api.responses.Response;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
class ContoCashControllerTest {

    @SpyBean
    private ContoCashController controller;

    @Test
    void testControllerIsNotNull() {
        assertNotNull(controller, "Warning: Controller should be Not Null.");
    }

    @Test
    void testContiCash() {

        // given
        String json = "{\n    \"status\": \"OK\",\n    \"error\": [],\n    \"payload\": [{\n" +
                "            \"id\": \"6123d97577c4201cc2226b9d\",\n            \"accountId\": \"14537780\",\n" +
                "            \"iban\": \"IT40L0326822311052923800661\",\n            \"abiCode\": \"03268\",\n" +
                "            \"cabCode\": \"22311\",\n            \"countryCode\": \"IT\",\n" +
                "            \"internationalCin\": \"40\",\n            \"nationalCin\": \"L\",\n" +
                "            \"account\": \"52923800661\",\n            \"alias\": \"Test api\",\n" +
                "            \"productName\": \"Conto Websella\",\n            \"holderName\": \"LUCA TERRIBILE\",\n" +
                "            \"activatedDate\": \"2016-12-14\",\n            \"currency\": \"EUR\"\n        }]\n}";
        ResponseEntity<Response> expected = new ResponseEntity(json, HttpStatus.OK);

        // when
        when(controller.contiCash()).thenReturn(expected);
        ResponseEntity<Response> actual = controller.contiCash();

        // then
        assertNotNull(actual, "Warning: Actual value should be Not Null.");
        assertEquals(expected.getBody(), actual.getBody(), "Warning: Invalid Response");
        verify(controller).contiCash();
    }
}