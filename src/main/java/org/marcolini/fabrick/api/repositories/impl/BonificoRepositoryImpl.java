/*
 * All Rights Reserved License, proprietary and confidential
 *
 * Copyright (c) 2021. Stefano Marcolini - Milano Italy, - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Stefano Marcolini stefano@marcolini.org, 2021.
 *
 * It is not allowed to reuse, modify, or redistribute the Software for
 * commercial use in any way.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS OR APRESS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package org.marcolini.fabrick.api.repositories.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.marcolini.fabrick.api.constants.ResponseConstants;
import org.marcolini.fabrick.api.documents.BonificoDocument;
import org.marcolini.fabrick.api.exceptions.MongoCustomException;
import org.marcolini.fabrick.api.repositories.apis.BonificoRepository;
import org.marcolini.fabrick.api.responses.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static org.marcolini.fabrick.api.utils.SafeUtil.isNotNull;

@Component
@Slf4j
public class BonificoRepositoryImpl {

    @Autowired
    private BonificoRepository repository;
    @Autowired
    private MongoTemplate mongoTemplate;
    @Autowired
    private ObjectMapper mapper;

    public void saveBonifico(Response response) {
        if (isNotNull(response) && ResponseConstants.OK.equalsIgnoreCase(response.getStatus())) {
            String json;
            BonificoDocument document;
            try {
                json = mapper.writeValueAsString(response.getPayload());
                document = mapper.readValue(json, BonificoDocument.class);
                repository.save(document);
            } catch (JsonProcessingException e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    public List<BonificoDocument> findAll() {
        try {
            return mongoTemplate.findAll(BonificoDocument.class);
        } catch (MongoCustomException e) {
            log.error(e.getMessage(), e);
        }
        return new ArrayList<>();
    }
}
