package org.marcolini.fabrick.api.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.marcolini.fabrick.api.models.Account;
import org.marcolini.fabrick.api.models.Creditor;
import org.marcolini.fabrick.api.requests.BonificoRequest;
import org.marcolini.fabrick.api.responses.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
class BonificoControllerTest {

    private static final long ACCOUNT_ID = 14537780L;

    @SpyBean
    private BonificoController controller;
    @Autowired
    private ObjectMapper mapper;

    @Test
    void testControllerIsNotNull() {
        assertNotNull(controller, "Warning: Controller should be Not Null.");
    }

    @Test
    void testBonifico() {

        // given
        BonificoRequest request = new BonificoRequest(new Creditor("LUCA TERRIBILE", new Account((
                "IT40L0326822311052923800661"))), "2021-08-23", "Payment invoice 75/2017", 
                800.0, "EUR");
        String json = "{\n    \"status\": \"KO\",\n    \"errors\": [\n        {\n" +
                "            \"code\": \"API000\",\n            \"description\": \"Il conto beneficiario non puo' " +
                "essere uguale a conto ordinante, di addebito o di addebito spese\",\n            \"params\": \"\"\n" +
                "        }\n    ],\n    \"payload\": {}\n}";
        ResponseEntity<Response> expected = new ResponseEntity(json, HttpStatus.BAD_REQUEST);

        // when
        when(controller.bonifico(ACCOUNT_ID, request)).thenReturn(expected);
        ResponseEntity<Response> actual = controller.bonifico(ACCOUNT_ID, request);

        // then
        assertNotNull(actual, "Warning: Actual value should be Not Null.");
        assertEquals(expected, actual, "Warning: Invalid Response");
        verify(controller).bonifico(ACCOUNT_ID, request);
    }

    @Test
    void testBonifici() {

        // given
        String json = "{\n    \"status\": \"OK\",\n    \"error\": [],\n    \"payload\": []\n}";
        ResponseEntity<Response> expected = new ResponseEntity(json, HttpStatus.OK);

        // when
        when(controller.bonifici()).thenReturn(expected);
        ResponseEntity<Response> actual = controller.bonifici();

        // then
        assertNotNull(actual, "Warning: Actual value should be Not Null.");
        assertEquals(expected, actual, "Warning: Invalid Response");
        verify(controller).bonifici();
    }
}