package org.marcolini.fabrick.api.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.marcolini.fabrick.api.responses.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
class SaldoControllerTest {

    private static final long ACCOUNT_ID = 14537780L;

    @SpyBean
    private SaldoController controller;
    @Autowired
    private ObjectMapper mapper;

    @Test
    void testControllerIsNotNull() {
        assertNotNull(controller, "Warning: Controller should be Not Null.");
    }

    @Test
    void testLetturaSaldo() throws IOException {

        // given
        String json = "{\n    \"status\": \"OK\",\n    \"error\": [],\n    \"payload\": {\n" +
                "        \"date\": \"2021-08-24\",\n        \"balance\": -0.8,\n        \"availableBalance\": -0.8,\n" +
                "        \"currency\": \"EUR\"\n    }\n}";
        Response response = mapper.readValue(json.getBytes(StandardCharsets.UTF_8), Response.class);
        ResponseEntity<Response> expected = new ResponseEntity<>(response, HttpStatus.OK);

        // when
        when(controller.letturaSaldo(ACCOUNT_ID)).thenReturn(expected);
        ResponseEntity<Response> actual = controller.letturaSaldo(ACCOUNT_ID);

        // then
        assertNotNull(actual, "Warning: Response should be Not Null.");
        assertEquals(expected, actual, "Warning: Invalid Response");
        verify(controller).letturaSaldo(ACCOUNT_ID);
    }
}