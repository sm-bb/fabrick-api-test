/*
 * All Rights Reserved License, proprietary and confidential
 *
 * Copyright (c) 2021. Stefano Marcolini - Milano Italy, - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Stefano Marcolini stefano@marcolini.org, 2021.
 *
 * It is not allowed to reuse, modify, or redistribute the Software for
 * commercial use in any way.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS OR APRESS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package org.marcolini.fabrick.api.controllers;

import org.marcolini.fabrick.api.repositories.impl.TransazioniRepositoryImpl;
import org.marcolini.fabrick.api.responses.Response;
import org.marcolini.fabrick.api.services.TransazioniService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.marcolini.fabrick.api.utils.SafeUtil.isNotNull;

@RestController
@RequestMapping("v1/api")
public class TransazioniController {

    @Autowired
    private TransazioniService service;
    @Autowired
    private TransazioniRepositoryImpl repository;

    @GetMapping(value = "/transactions")
    public ResponseEntity<Response> letturaTransazioni() {
        

        ResponseEntity<Response> response = service.getTransazioni();

        if (isNotNull(response) && response.getStatusCode().is2xxSuccessful()) {
            repository.saveBonifico(response.getBody());
        }

        return response;
    }
}
