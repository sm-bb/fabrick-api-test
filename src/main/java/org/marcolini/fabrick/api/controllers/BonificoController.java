/*
 * All Rights Reserved License, proprietary and confidential
 *
 * Copyright (c) 2021. Stefano Marcolini - Milano Italy, - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Stefano Marcolini stefano@marcolini.org, 2021.
 *
 * It is not allowed to reuse, modify, or redistribute the Software for
 * commercial use in any way.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS OR APRESS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package org.marcolini.fabrick.api.controllers;

import lombok.extern.slf4j.Slf4j;
import org.marcolini.fabrick.api.documents.BonificoDocument;
import org.marcolini.fabrick.api.repositories.impl.BonificoRepositoryImpl;
import org.marcolini.fabrick.api.requests.BonificoRequest;
import org.marcolini.fabrick.api.responses.Response;
import org.marcolini.fabrick.api.services.BonificoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

import static org.marcolini.fabrick.api.utils.SafeUtil.isNotNull;

@RestController
@RequestMapping("v1/api")
@Slf4j
public class BonificoController {

    @Autowired
    private BonificoService service;
    @Autowired
    private BonificoRepositoryImpl repository;

    @PostMapping(value = "/bonifico/{accountId}")
    public ResponseEntity<Response> bonifico(@PathVariable(name = "accountId") Long accountId,
                                             @RequestBody @NonNull BonificoRequest request) {
        ResponseEntity<Response> response = service.postToBonificoFabrickApi(accountId, request);

        if (isNotNull(response) && response.getStatusCode().is2xxSuccessful()) {
            repository.saveBonifico(response.getBody());
        }

        return response;
    }

    @GetMapping(value = "/bonifici")
    public ResponseEntity<Response> bonifici() {

        List<BonificoDocument> list = repository.findAll();

        var response = Response.builder()
                .status(HttpStatus.OK.name())
                .error(new String[]{})
                .payload(list)
                .build();

        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
