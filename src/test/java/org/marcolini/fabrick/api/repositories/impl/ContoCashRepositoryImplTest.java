package org.marcolini.fabrick.api.repositories.impl;

import org.junit.jupiter.api.Test;
import org.marcolini.fabrick.api.documents.ContoCashDocument;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
class ContoCashRepositoryImplTest {

    @MockBean
    private ContoCashRepositoryImpl repository;

    @Test
    void testRepositoryIsNotNull() {
        assertNotNull(repository, "Warning: Controller should be Not Null.");
    }

    @Test
    void testFindAll() {

        // given
        List<ContoCashDocument> list = new ArrayList<>();

        // when
        when(repository.findAll()).thenReturn(list);
        repository.findAll();

        // then
        verify(repository).findAll();
    }
}