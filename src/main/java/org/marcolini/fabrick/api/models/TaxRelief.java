/*
 * All Rights Reserved License, proprietary and confidential
 *
 * Copyright (c) 2021. Stefano Marcolini - Milano Italy, - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Stefano Marcolini stefano@marcolini.org, 2021.
 *
 * It is not allowed to reuse, modify, or redistribute the Software for
 * commercial use in any way.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS OR APRESS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package org.marcolini.fabrick.api.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.marcolini.fabrick.api.enums.BeneficiaryType;
import org.marcolini.fabrick.api.enums.TaxReliefId;
import org.springframework.lang.NonNull;

@Getter
@NoArgsConstructor
public final class TaxRelief {

    private String taxReliefId;
    private Boolean isCondoUpgrade;     // required
    private String creditorFiscalCode;  // required : 56258745832
    private String beneficiaryType;     // required : NATURAL_PERSON | LEGAL_PERSON
    private NaturalPersonBeneficiary naturalPersonBeneficiary;
    private LegalPersonBeneficiary legalPersonBeneficiary;

    public TaxRelief(String taxReliefId, @NonNull Boolean isCondoUpgrade, @NonNull String creditorFiscalCode,
                     @NonNull String beneficiaryType, NaturalPersonBeneficiary naturalPersonBeneficiary,
                     LegalPersonBeneficiary legalPersonBeneficiary) {
        this.taxReliefId = TaxReliefId.validate(taxReliefId);
        this.isCondoUpgrade = isCondoUpgrade;
        this.creditorFiscalCode = creditorFiscalCode;
        this.beneficiaryType = BeneficiaryType.validate(beneficiaryType);
        this.naturalPersonBeneficiary = naturalPersonBeneficiary;
        this.legalPersonBeneficiary = legalPersonBeneficiary;
    }
}
