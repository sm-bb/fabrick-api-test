/*
 * All Rights Reserved License, proprietary and confidential
 *
 * Copyright (c) 2021. Stefano Marcolini - Milano Italy, - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Stefano Marcolini stefano@marcolini.org, 2021.
 *
 * It is not allowed to reuse, modify, or redistribute the Software for
 * commercial use in any way.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS OR APRESS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package org.marcolini.fabrick.api.documents;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.marcolini.fabrick.api.enums.Direction;
import org.marcolini.fabrick.api.enums.FeeType;
import org.marcolini.fabrick.api.enums.Status;
import org.marcolini.fabrick.api.models.Amount;
import org.marcolini.fabrick.api.models.Creditor;
import org.marcolini.fabrick.api.models.Debitor;
import org.marcolini.fabrick.api.models.Fee;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Document(collection = "bonifico")
@NoArgsConstructor
@Data
@Builder
public class BonificoDocument {

    @Id
    private String id;

    private String moneyTransferId;
    private String status;              // EXECUTED	| BOOKED | WORK_IN_PROGRESS	| CANCELLED	| REJECTED
    private String direction;           // INCOMING | OUTGOING
    private Creditor creditor;
    private Debitor debitor;
    private String cro;
    private String trn;
    private String description;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime createdDatetime;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime accountedDatetime;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate debtorValueDate;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate creditorValueDate;
    private Amount amount;
    private Boolean isUrgent;
    private Boolean isInstant;
    private String feeType;            // SHA | OUR | BEN
    private String feeAccountId;
    private Fee[] fees;
    private Boolean hasTaxRelief;


    public BonificoDocument(String id, String moneyTransferId, String status, String direction, Creditor creditor,
                    Debitor debitor, String cro, String trn, String description, LocalDateTime createdDatetime,
                    LocalDateTime accountedDatetime, LocalDate debtorValueDate, LocalDate creditorValueDate,
                    Amount amount, Boolean isUrgent, Boolean isInstant, String feeType, String feeAccountId,
                    Fee[] fees, Boolean hasTaxRelief) {
        this.id = id;
        this.moneyTransferId = moneyTransferId;
        this.status = Status.validate(status);
        this.direction = Direction.validate(direction);
        this.creditor = creditor;
        this.debitor = debitor;
        this.cro = cro;
        this.trn = trn;
        this.description = description;
        this.createdDatetime = createdDatetime;
        this.accountedDatetime = accountedDatetime;
        this.debtorValueDate = debtorValueDate;
        this.creditorValueDate = creditorValueDate;
        this.amount = amount;
        this.isUrgent = isUrgent;
        this.isInstant = isInstant;
        this.feeType = FeeType.validate(feeType);
        this.feeAccountId = feeAccountId;
        this.fees = fees.clone();
        this.hasTaxRelief = hasTaxRelief;
    }
}
