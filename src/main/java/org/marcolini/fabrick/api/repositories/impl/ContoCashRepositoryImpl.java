package org.marcolini.fabrick.api.repositories.impl;

import lombok.extern.slf4j.Slf4j;
import org.marcolini.fabrick.api.documents.ContoCashDocument;
import org.marcolini.fabrick.api.exceptions.MongoCustomException;
import org.marcolini.fabrick.api.repositories.apis.ContoCashRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class ContoCashRepositoryImpl {

    @Autowired
    private ContoCashRepository repository;
    @Autowired
    private MongoTemplate mongoTemplate;

    public List<ContoCashDocument> findAll() {
        try {
            return mongoTemplate.findAll(ContoCashDocument.class);
        } catch (MongoCustomException e) {
            log.error(e.getMessage(), e);
        }
        return new ArrayList<>();
    }
}
