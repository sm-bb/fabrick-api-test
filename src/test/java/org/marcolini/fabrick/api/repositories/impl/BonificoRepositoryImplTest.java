package org.marcolini.fabrick.api.repositories.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.marcolini.fabrick.api.documents.BonificoDocument;
import org.marcolini.fabrick.api.responses.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
class BonificoRepositoryImplTest {

    @MockBean
    private BonificoRepositoryImpl repository;
    @Autowired
    private MongoTemplate mongoTemplate;
    @Autowired
    private ObjectMapper mapper;

    @Test
    void testRepositoryIsNotNull() {
        assertNotNull(repository, "Warning: Controller should be Not Null.");
    }

    @Test
    void testSaveBonifico() {

        // given
        String json = "{\n    \"status\": \"KO\",\n    \"errors\": [\n        {\n" +
                "            \"code\": \"API000\",\n            \"description\": \"Il conto beneficiario non puo' " +
                "essere uguale a conto ordinante, di addebito o di addebito spese\",\n            \"params\": \"\"\n" +
                "        }\n    ],\n    \"payload\": {}\n}";
        ResponseEntity<Response> response = new ResponseEntity(json, HttpStatus.BAD_REQUEST);
        Response expected = Response.builder()
                .status("KO")
                .error(new String[]{"\"code\": \"API000\",\"description\": \"Il conto beneficiario non puo' \" +\n" +
                        "\"essere uguale a conto ordinante, di addebito o di addebito spese\",\"params\": \"\""})
                .payload("{}")
                .build();

        // when
        doNothing().when(repository).saveBonifico(expected);
        repository.saveBonifico(expected);
        // then
        verify(repository).saveBonifico(expected);
    }

    @Test
    void testFindAll() {

        // given
        List<BonificoDocument> list = new ArrayList<>();

        // when
        when(repository.findAll()).thenReturn(list);
        repository.findAll();

        // then
        verify(repository).findAll();
    }
}