/*
 * All Rights Reserved License, proprietary and confidential
 *
 * Copyright (c) 2021. Stefano Marcolini - Milano Italy, - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Stefano Marcolini stefano@marcolini.org, 2021.
 *
 * It is not allowed to reuse, modify, or redistribute the Software for
 * commercial use in any way.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS OR APRESS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package org.marcolini.fabrick.api.services;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.marcolini.fabrick.api.models.*;
import org.marcolini.fabrick.api.requests.BonificoRequest;
import org.marcolini.fabrick.api.responses.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
class BonificoServiceTest {

    @Autowired
    private ObjectMapper mapper;

    @SpyBean
    private BonificoService service;

    private Long accountId;
    private BonificoRequest bonificoRequest;

    @BeforeEach
    void setup() {
        accountId = 14537780L;
        bonificoRequest = initBonificoRequest();
    }

    private BonificoRequest initBonificoRequest() {
        var name = "LUCA TERRIBILE";         // required
        Account account = initAccount();             // required
        Creditor creditor = new Creditor(name, account);
        var executionDate = "2021-08-23";
        //var uri = "";
        var description = "Payment invoice 75/2017";
        var amount = 800.0;
        var currency = "EUR";
        return new BonificoRequest(creditor, executionDate, description, amount, currency);
    }

    private Account initAccount() {
        var accountCode = "IT40L0326822311052923800661";     //required
        return new Account(accountCode);
    }

    @Test
    void testServiceIsNotNull() {
        assertNotNull(service, "Warning: Service should be Not Null.");
    }

    @Test
    void testPostToBonificoFabrickApi() {

        // given
        mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        ReflectionTestUtils.setField(service, "domain", "https://sandbox.platfr.io");
        ReflectionTestUtils.setField(service, "endpoint", "api/gbs/banking/v4.0/accounts/%s/payments" +
                "/money-transfers");
        String json = "{\n     \"status\": \"KO\",\n" +
                "    \"errors\": [\n         {\n             \"code\": \"API000\",\n" +
                "            \"description\": \"Il conto beneficiario non puo' essere uguale a conto ordinante, di " +
                "addebito o di addebito spese\",\n             \"params\": \"\"\n         }\n     ],\n" +
                "    \"payload\": {}\n }";
        ResponseEntity<Response> expected = new ResponseEntity(json, HttpStatus.BAD_REQUEST);

        // when
        when(service.postToBonificoFabrickApi(accountId, bonificoRequest)).thenReturn(expected);

        var response = service.postToBonificoFabrickApi(accountId, bonificoRequest);

        // then
        assertNotNull(response, "Warning: Response should be Not Null.");
        verify(service).postToBonificoFabrickApi(accountId, bonificoRequest);

    }
}