/*
 * All Rights Reserved License, proprietary and confidential
 *
 * Copyright (c) 2021. Stefano Marcolini - Milano Italy, - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Stefano Marcolini stefano@marcolini.org, 2021.
 *
 * It is not allowed to reuse, modify, or redistribute the Software for
 * commercial use in any way.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS OR APRESS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package org.marcolini.fabrick.api.services;

import lombok.extern.slf4j.Slf4j;
import org.marcolini.fabrick.api.constants.ApiConstants;
import org.marcolini.fabrick.api.properties.ApiProperties;
import org.marcolini.fabrick.api.responses.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
public class LetturaSaldoService {

    private RestTemplate restTemplate;
    private ApiProperties properties;

    @Value("${fabrick.domain.sandbox}")
    private String domain;
    @Value("${fabrick.endpoint.lettura-saldo}")
    private String endpoint;

    @Autowired
    public LetturaSaldoService(RestTemplate restTemplate, ApiProperties properties) {
        this.restTemplate = restTemplate;
        this.properties = properties;
    }

    public ResponseEntity<Response> getLetturaSaldo(Long accountId) {

        var uri = String.format("%s/%s", domain, String.format(endpoint, accountId));
        var headers = httpHeaders();
        HttpEntity<String> entity = new HttpEntity<>(headers);

        return getLetturaSaldo(uri, entity);
    }

    private ResponseEntity<Response> getLetturaSaldo(String uri, HttpEntity<String> entity) {
        HttpStatus status;
        Response response;
        try {
            return restTemplate.exchange(
                    uri,
                    HttpMethod.GET,
                    entity,
                    Response.class);
        } catch(HttpClientErrorException e) {
            status = e.getStatusCode();
            var message = String.format("%s %s", status, e.getStatusText());
            response = new Response(String.valueOf(e.getRawStatusCode()), new String[] {e.getStatusText()},
                    e.getResponseBodyAsString());
            log.error(message, e);
        }
        return new ResponseEntity(response.getPayload(), status);
    }

    private HttpHeaders httpHeaders() {
        var headers = new HttpHeaders();
        headers.add(ApiConstants.API_KEY, properties.getApiKey());
        headers.add(ApiConstants.AUTH_SCHEMA, properties.getAuthSchema());
        headers.add(ApiConstants.TIME_ZONE, properties.getTimeZone());
        headers.add(ApiConstants.CONTENT_TYPE, ApiConstants.APPLICATION_JSON);
        return headers;
    }
}
