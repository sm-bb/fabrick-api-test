/*
 * All Rights Reserved License, proprietary and confidential
 *
 * Copyright (c) 2021. Stefano Marcolini - Milano Italy, - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Stefano Marcolini stefano@marcolini.org, 2021.
 *
 * It is not allowed to reuse, modify, or redistribute the Software for
 * commercial use in any way.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS OR APRESS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package org.marcolini.fabrick.api.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.marcolini.fabrick.api.constants.ApiConstants;
import org.marcolini.fabrick.api.properties.ApiProperties;
import org.marcolini.fabrick.api.requests.BonificoRequest;
import org.marcolini.fabrick.api.responses.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
public class BonificoService {

    private static final String REGEX = "(,)[\"a-zA-Z]*(:null)";
    private static final String EMPTY = "";

    private RestTemplate restTemplate;
    private ApiProperties properties;
    private ObjectMapper mapper;

    @Value("${fabrick.domain.sandbox}")
    private String domain;
    @Value("${fabrick.endpoint.bonifico}")
    private String endpoint;

    @Autowired
    public BonificoService(RestTemplate restTemplate, ApiProperties properties, ObjectMapper mapper) {
        this.restTemplate = restTemplate;
        this.properties = properties;
        this.mapper = mapper;
    }

    public ResponseEntity<Response> postToBonificoFabrickApi(Long accountId, BonificoRequest body) {

        var uri = String.format("%s/%s", domain, String.format(endpoint, accountId));
        var headers = httpHeaders();
        var json = toJsonString(body);
        HttpEntity<String> entity = new HttpEntity<>(json, headers);

        return postToBonifico(uri, entity);
    }

    private String toJsonString(BonificoRequest body) {
        try {
            var json = mapper.writeValueAsString(body);
            return json.replaceAll(REGEX, EMPTY);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage(), e);
        }
        return EMPTY;
    }

    private ResponseEntity<Response> postToBonifico(String uri, HttpEntity<String> entity) {
        HttpStatus status;
        Response response;
        try {
            return restTemplate.exchange(
                    uri,
                    HttpMethod.POST,
                    entity,
                    Response.class);
        } catch(HttpClientErrorException e) {
            status = e.getStatusCode();
            var message = String.format("%s %s", status, e.getStatusText());
            response = new Response(String.valueOf(e.getRawStatusCode()), new String[] {e.getStatusText()},
                    e.getResponseBodyAsString());
            log.error(message, e);
        }
        return new ResponseEntity(response.getPayload(), status);
    }

    private HttpHeaders httpHeaders() {
        var headers = new HttpHeaders();
        headers.add(ApiConstants.API_KEY, properties.getApiKey());
        headers.add(ApiConstants.AUTH_SCHEMA, properties.getAuthSchema());
        headers.add(ApiConstants.TIME_ZONE, properties.getTimeZone());
        headers.add(ApiConstants.CONTENT_TYPE, ApiConstants.APPLICATION_JSON);
        return headers;
    }
}
