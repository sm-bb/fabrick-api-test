package org.marcolini.fabrick.api.controllers;

import org.marcolini.fabrick.api.documents.ContoCashDocument;
import org.marcolini.fabrick.api.repositories.impl.ContoCashRepositoryImpl;
import org.marcolini.fabrick.api.responses.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("v1/api")
public class ContoCashController {

    @Autowired
    private ContoCashRepositoryImpl repository;

    @GetMapping(value = "/conticash")
    public ResponseEntity<Response> contiCash() {

        List<ContoCashDocument> list = repository.findAll();

        var response = Response.builder()
                                    .status(HttpStatus.OK.name())
                                    .error(new String[]{})
                                    .payload(list)
                                    .build();

        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
