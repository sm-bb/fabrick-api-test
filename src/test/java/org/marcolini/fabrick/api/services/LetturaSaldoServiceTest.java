/*
 * All Rights Reserved License, proprietary and confidential
 *
 * Copyright (c) 2021. Stefano Marcolini - Milano Italy, - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Stefano Marcolini stefano@marcolini.org, 2021.
 *
 * It is not allowed to reuse, modify, or redistribute the Software for
 * commercial use in any way.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS OR APRESS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package org.marcolini.fabrick.api.services;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.marcolini.fabrick.api.responses.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
class LetturaSaldoServiceTest {

    @Autowired
    private ObjectMapper mapper;

    @SpyBean
    private LetturaSaldoService service;

    private Long accountId;

    @BeforeEach
    void setup() {
        accountId = 14537780L;
    }

    @Test
    void testServiceIsNotNull() {
        assertNotNull(service, "Warning: Service should Not be Null");
    }

    @Test
    void testGetLetturaSaldo() throws IOException {


        // given
        mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        ReflectionTestUtils.setField(service, "domain", "https://sandbox.platfr.io");
        ReflectionTestUtils.setField(service, "endpoint", "api/gbs/banking/v4.0/accounts/%d/balance");
        String json = "{\n    \"status\": \"OK\",\n    \"error\": [],\n    \"payload\": {\n" +
                "        \"date\": \"2021-08-24\",\n        \"balance\": -0.8,\n        \"availableBalance\": -0.8,\n" +
                "        \"currency\": \"EUR\"\n    }\n}";
        Response response = mapper.readValue(json.getBytes(StandardCharsets.UTF_8), Response.class);
        ResponseEntity<Response> expected = new ResponseEntity<>(response, HttpStatus.OK);

        // when
        when(service.getLetturaSaldo(accountId)).thenReturn(expected);

        var actual = service.getLetturaSaldo(accountId);

        // then
        assertNotNull(actual, "Warning: Response should Not be Null");
        assertEquals(expected, actual, "Warning: Invalid Response");
        verify(service).getLetturaSaldo(accountId);

    }
}